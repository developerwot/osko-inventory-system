<?php namespace App\Repositories\Backend\Profile;

use App\User;
use Illuminate\Support\Facades\Storage;

class ProfileRepository implements ProfileContracts
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function edit()
    {
        return $this->model->findOrFail(auth()->id());
    }

    public function update($input)
    {
        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        } else {
            unset($input['password']);
        }

        $user = $this->model->findOrFail(auth()->id());
        return $user->update($input);
    }

}
