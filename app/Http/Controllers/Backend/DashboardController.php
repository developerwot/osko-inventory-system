<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Dashboard\DashboardContract;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    protected $repository;

    public function __construct(DashboardContract $repository)
    {
        $this->repository = $repository;
    }

    //return all the reservation record from database
    public function index()
    {
        try {
            $user_count = User::all()->count();
            return view('backend.dashboard', compact('user_count'));

        } catch (\Exception $e) {
            Log::error('Dashboard error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');

        }
    }


}
