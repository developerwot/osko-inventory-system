<?php

namespace App\Http\Controllers\Auth;

use App\Repositories\Backend\Profile\ProfileContracts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;


class ProfileController extends Controller
{
    protected $repository;

    public function __construct(ProfileContracts $repository)
    {
        $this->repository = $repository;
    }

    public function editProfile()
    {
        try {
            $user = $this->repository->edit();
            return view('backend.auth.profile', compact('user'));
        } catch (\Exception $e) {
            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }
    }

    public function updateProfile(Request $request)
    {
        try {
            $input = $request->only('name', 'password');
            $this->repository->update($input);
            return redirect()->route('edit.profile')->with('success', 'Profile updated successfully');

        } catch (\Exception $e) {

            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }

    }
}
