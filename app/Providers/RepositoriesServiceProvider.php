<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->apiFrontendRepositoryRegister();
        $this->backendRepositoryRegister();
    }

    public function apiFrontendRepositoryRegister()
    {

    }

    public function backendRepositoryRegister()
    {
        $this->app->bind(
            \App\Repositories\Backend\User\UserContract::class,
            \App\Repositories\Backend\User\UserRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Dashboard\DashboardContract::class,
            \App\Repositories\Backend\Dashboard\DashboardRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Profile\ProfileContracts::class,
            \App\Repositories\Backend\Profile\ProfileRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Role\RoleContract::class,
            \App\Repositories\Backend\Role\RoleRepository::class
        );

    }
}
