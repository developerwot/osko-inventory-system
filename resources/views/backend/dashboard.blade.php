@extends('backend.layouts.master')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">
                        Dashboard
                    </h3>
                </div>

            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div
                class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light  m-portlet--rounded-force">

                <div class="m-portlet__body">
                    <div class="m-widget17">
                        <div
                            class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                            <div class="m-widget17__chart" style="height:320px;">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div></div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink">
                                        <div></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="m-widget17__stats">
                            <div class="m-widget17__items m-widget17__items-col1">
                                <div class="m-widget17__item">
                                <span class="m-widget17__icon">
                                    <i class="flaticon-users m--font-brand"></i>
                                </span>
                                    <span class="m-widget17__subtitle">
                                   Users
                                </span>
                                    <span class="m-widget17__desc">
                                    ---
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget17__items m-widget17__items-col2">
                                <div class="m-widget17__item">
                                    <span class="m-widget17__icon">
                                        <i class="flaticon-user-settings m--font-success"></i>
                                    </span>
                                    <span class="m-widget17__subtitle">
                                    Roles
                                    </span>
                                    <span class="m-widget17__desc">
                                        ---
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
