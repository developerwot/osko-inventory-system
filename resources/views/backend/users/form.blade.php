@extends('backend.layouts.master')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title">
                        {{(isset($user))? 'Edit User' : 'Add User' }}
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-content" style="width: 100%">
            <div class="m-portlet m-portlet--tab">
                <form id="form" class="m-form m-form--fit m-form--label-align-right"
                      action="{{ isset($user) ? route('users.update', $user->id) : route('users.store')}}"
                      method="POST" enctype="multipart/form-data">
                    @csrf
                    @if(isset($user))
                        @method('PUT')
                    @endif
                    <div class="m-portlet__body">

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Name*
                                    </label>
                                    <input type="text" class="form-control m-input" name="name"
                                           placeholder="Enter name"
                                           value="{!! isset($user) ? $user->name : old('name') !!}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Email*
                                    </label>
                                    <input type="text" class="form-control m-input" name="email"
                                           placeholder="Enter email"
                                           value="{!! isset($user) ? $user->email : old('email') !!}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Select User Role*
                                    </label>
                                    <select class="form-control m-input" name="roles[]">
                                        <option value="" disabled selected>Select user role</option>
                                        @if(isset($roles) &&  count($roles) > 0)
                                            @foreach($roles as $role)
                                                <option
                                                    value="{{ $role->id }}" @if(isset($user) && $user->hasRole($role->name)) selected @endif>
                                                    {{ $role->display_name }}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>


@endsection
@section('scripts')

    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            $('#form').validate({
                rules: {
                    name: "required",
                    'roles[]': "required",
                    email: {
                        required: true,
                        email: true
                    },

                },
                messages: {
                    name: "Name field is required",
                    'roles[]': "Please select any role",
                    email: {
                        required: "Email Field is required",
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
