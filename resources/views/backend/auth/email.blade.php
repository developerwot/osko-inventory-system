@extends('backend.layouts.auth_master')
@section('content')

    <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Forgotten Password ?
                </h3>
                <div class="m-login__desc">
                    Enter your email to reset your password:
                </div>
            </div>
        <form id="form" method="POST" class="m-login__form m-form"
              action="{{ route('password.email') }}">
            @csrf
            <div class="form-group m-form__group">
                <input class="form-control m-input" type="text" placeholder="Email" name="email"
                       id="m_email" autocomplete="off"  value="{{ old('email') }}">
            </div>
            <div class="m-login__form-action">
                <button type="submit"
                        class="btn btn-primary btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                    Request
                </button>


                <a href="{{ route('login') }}"
                   class="btn btn-outline-primary m-btn m-btn--pill m-btn--custom">
                    Cancel
                </a>


            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            $('#form').validate({
                rules: {
                    email: {
                        email: true,
                        required: true
                    },

                },messages: {
                    email: {
                        required: "Email field is required",
                        email: "Please use valid email format"
                    },
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
