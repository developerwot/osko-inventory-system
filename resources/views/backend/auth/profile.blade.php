@extends('backend.layouts.master')
@section('content')


    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content" style="width: 100%;">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Update Profile
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <form id="form" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                      method="POST" enctype="multipart/form-data"
                      action="{{ route('update.profile', $user->id) }}">

                    @csrf

                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Name*
                                    </label>
                                    <input type="text" class="form-control m-input" name="name"
                                           placeholder="Enter name"
                                           value="{!! isset($user) ? $user->name : old('name') !!}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Email*
                                    </label>
                                    <input type="text" class="form-control m-input" name="email" placeholder="Enter email"
                                           value="{!! isset($user) ? $user->email : old('email') !!}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Password
                                    </label>
                                    <input type="password" class="form-control m-input" name="password" id="password"
                                           placeholder="Enter password">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Confirm Password
                                    </label>
                                    <input type="password" class="form-control m-input" name="confirm_password"
                                           placeholder="Confirm password">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });

            $('#form').validate({
                rules: {
                    name: "required",
                    email: "required",
                    password: {
                        minlength: 6,
                    },
                    confirm_password: {
                        minlength: 6,
                        equalTo: "#password"
                    }

                },
                messages: {},
                submitHandler: function (form) {
                    form.submit();
                }, errorPlacement: function (error, element) {
                    console.log(element);
                    element.parent().append(error);
                }
            });
        });
    </script>


@endsection



