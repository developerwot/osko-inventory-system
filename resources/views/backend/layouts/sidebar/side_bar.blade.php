<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            {{--<li class="m-menu__item {{ isActiveRoute('dashboard') }}" aria-haspopup="true">--}}
            {{--<a href="{{ route('dashboard') }}" class="m-menu__link ">--}}
            {{--<i class="m-menu__link-icon flaticon-line-graph"></i>--}}
            {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
            {{--<span class="m-menu__link-text">--}}
            {{--Dashboard--}}
            {{--</span>--}}
            {{--</span>--}}
            {{--</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="m-menu__section">--}}
            {{--<h4 class="m-menu__section-text">--}}
            {{--@lang('dashboard.management')--}}
            {{--</h4>--}}
            {{--<i class="m-menu__section-icon flaticon-more-v3"></i>--}}
            {{--</li>--}}
            <li class="m-menu__item  m-menu__item--submenu {{ areActiveRoutes(["users.index", "users.create","users.show","users.edit"]) }}"
                aria-haspopup="true">
                <a href="{{ route('users.index') }}" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-text">
                        Users
                    </span>
{{--                    <i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                </a>
            </li>

            <li class="m-menu__item  m-menu__item--submenu {{ areActiveRoutes(["roles.index", "roles.create","roles.show","roles.edit"]) }}"
                aria-haspopup="true">
                <a href="{{ route('roles.index') }}" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-user-settings"></i>
                    <span class="m-menu__link-text">
                        Roles
                    </span>
                    {{--                    <i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                </a>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
