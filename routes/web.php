<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', function () {
    if (auth()->check()) {
        return redirect('/dashboard');
    } else {
        return redirect('/login');
    }
});

Route::get('/home', function () {
    return redirect('/dashboard');
});

/*
   |--------------------------------------------------------------------------
   | Routes Without authentication
   |--------------------------------------------------------------------------
 */
Route::get('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/forgot', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password_reset_save');
Route::post('/reset/password', 'Auth\ResetPasswordController@reset')->name('password.reset');


Auth::routes();

Route::group(['middleware' => 'auth'], function () {


    Route::group(['namespace' => 'Auth'], function () {

        Route::get('update-profile', 'ProfileController@editProfile')->name('edit.profile');
        Route::post('update-profile', 'ProfileController@updateProfile')->name('update.profile');

        Route::get('logout', 'LoginController@logout')->name('logout');
    });

    /*
      |--------------------------------------------------------------------------
      | Super Admin Routes
      |--------------------------------------------------------------------------
    */
    Route::group(['namespace' => 'Backend'], function () {

        //After Login redirect to dashboard
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        Route::resource('users', 'UserController');
        Route::get('users/delete/{id}', ['as' => 'users.delete', 'uses' => 'UserController@destroy']);

        Route::resource('roles', 'RoleController');
        Route::get('roles/delete/{id}', ['as' => 'roles.delete', 'uses' => 'RoleController@destroy']);


    });
});


