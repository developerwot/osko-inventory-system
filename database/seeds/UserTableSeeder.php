<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt('123456');
        $data = [
            [
                'name' =>'Super admin',
                'email' => 'admin@mailinator.com',
                'password' => $password,
            ]
        ];


        \App\User::truncate();

        foreach ($data as $user){
            $user = \App\User::firstOrCreate($user);
            $user->syncRoles([1]);
        }
    }
}
